package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Joke;
import com.mycompany.myapp.repository.JokeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link JokeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class JokeResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Float DEFAULT_RATING = 1F;
    private static final Float UPDATED_RATING = 2F;

    private static final String ENTITY_API_URL = "/api/jokes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private JokeRepository jokeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJokeMockMvc;

    private Joke joke;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joke createEntity(EntityManager em) {
        Joke joke = new Joke().title(DEFAULT_TITLE).content(DEFAULT_CONTENT).rating(DEFAULT_RATING);
        return joke;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Joke createUpdatedEntity(EntityManager em) {
        Joke joke = new Joke().title(UPDATED_TITLE).content(UPDATED_CONTENT).rating(UPDATED_RATING);
        return joke;
    }

    @BeforeEach
    public void initTest() {
        joke = createEntity(em);
    }

    @Test
    @Transactional
    void createJoke() throws Exception {
        int databaseSizeBeforeCreate = jokeRepository.findAll().size();
        // Create the Joke
        restJokeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(joke)))
            .andExpect(status().isCreated());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeCreate + 1);
        Joke testJoke = jokeList.get(jokeList.size() - 1);
        assertThat(testJoke.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testJoke.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testJoke.getRating()).isEqualTo(DEFAULT_RATING);
    }

    @Test
    @Transactional
    void createJokeWithExistingId() throws Exception {
        // Create the Joke with an existing ID
        joke.setId(1L);

        int databaseSizeBeforeCreate = jokeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restJokeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(joke)))
            .andExpect(status().isBadRequest());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = jokeRepository.findAll().size();
        // set the field null
        joke.setTitle(null);

        // Create the Joke, which fails.

        restJokeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(joke)))
            .andExpect(status().isBadRequest());

        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllJokes() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        // Get all the jokeList
        restJokeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(joke.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.doubleValue())));
    }

    @Test
    @Transactional
    void getJoke() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        // Get the joke
        restJokeMockMvc
            .perform(get(ENTITY_API_URL_ID, joke.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(joke.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.doubleValue()));
    }

    @Test
    @Transactional
    void getNonExistingJoke() throws Exception {
        // Get the joke
        restJokeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewJoke() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();

        // Update the joke
        Joke updatedJoke = jokeRepository.findById(joke.getId()).get();
        // Disconnect from session so that the updates on updatedJoke are not directly saved in db
        em.detach(updatedJoke);
        updatedJoke.title(UPDATED_TITLE).content(UPDATED_CONTENT).rating(UPDATED_RATING);

        restJokeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedJoke.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedJoke))
            )
            .andExpect(status().isOk());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
        Joke testJoke = jokeList.get(jokeList.size() - 1);
        assertThat(testJoke.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testJoke.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testJoke.getRating()).isEqualTo(UPDATED_RATING);
    }

    @Test
    @Transactional
    void putNonExistingJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, joke.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(joke))
            )
            .andExpect(status().isBadRequest());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(joke))
            )
            .andExpect(status().isBadRequest());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(joke)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateJokeWithPatch() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();

        // Update the joke using partial update
        Joke partialUpdatedJoke = new Joke();
        partialUpdatedJoke.setId(joke.getId());

        partialUpdatedJoke.rating(UPDATED_RATING);

        restJokeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedJoke.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedJoke))
            )
            .andExpect(status().isOk());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
        Joke testJoke = jokeList.get(jokeList.size() - 1);
        assertThat(testJoke.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testJoke.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testJoke.getRating()).isEqualTo(UPDATED_RATING);
    }

    @Test
    @Transactional
    void fullUpdateJokeWithPatch() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();

        // Update the joke using partial update
        Joke partialUpdatedJoke = new Joke();
        partialUpdatedJoke.setId(joke.getId());

        partialUpdatedJoke.title(UPDATED_TITLE).content(UPDATED_CONTENT).rating(UPDATED_RATING);

        restJokeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedJoke.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedJoke))
            )
            .andExpect(status().isOk());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
        Joke testJoke = jokeList.get(jokeList.size() - 1);
        assertThat(testJoke.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testJoke.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testJoke.getRating()).isEqualTo(UPDATED_RATING);
    }

    @Test
    @Transactional
    void patchNonExistingJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, joke.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(joke))
            )
            .andExpect(status().isBadRequest());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(joke))
            )
            .andExpect(status().isBadRequest());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamJoke() throws Exception {
        int databaseSizeBeforeUpdate = jokeRepository.findAll().size();
        joke.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restJokeMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(joke)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Joke in the database
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteJoke() throws Exception {
        // Initialize the database
        jokeRepository.saveAndFlush(joke);

        int databaseSizeBeforeDelete = jokeRepository.findAll().size();

        // Delete the joke
        restJokeMockMvc
            .perform(delete(ENTITY_API_URL_ID, joke.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Joke> jokeList = jokeRepository.findAll();
        assertThat(jokeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
