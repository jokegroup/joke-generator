import { Component } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IJoke, Joke } from '../joke.model';
import { JokeService } from '../service/joke.service';
import { ICategory } from '../../category/category.model';
import { CategoryService } from '../../category/service/category.service';


@Component({
  selector: 'jhi-joke-modal',
  templateUrl: './joke-modal.component.html',
})
export class JokeModalComponent {
 
    isSaving = false;

    categoriesSharedCollection: ICategory[] = [];

    editForm = this.fb.group({
        id: [],
        title: [null, [Validators.required, Validators.maxLength(255)]],
        content: [null, [Validators.maxLength(500)]],
        rating: [],
        category: [],
      });
    
  constructor( protected jokeService: JokeService,private activeModal: NgbActiveModal,protected categoryService: CategoryService,protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}


  ngOnInit(): void {
   
      this.loadRelationshipsOptions();
  
  }
  dismiss(): void {
    this.activeModal.dismiss();
  }


  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const joke = this.createFromForm();
 
      this.subscribeToSaveResponse(this.jokeService.create(joke));
    
  }

  trackCategoryById(_index: number, item: ICategory): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IJoke>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.dismiss(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(joke: IJoke): void {
    this.editForm.patchValue({
      id: joke.id,
      title: joke.title,
      content: joke.content,
      rating: joke.rating,
      category: joke.category,
    });

    this.categoriesSharedCollection = this.categoryService.addCategoryToCollectionIfMissing(this.categoriesSharedCollection, joke.category);
  }

  protected loadRelationshipsOptions(): void {
    this.categoryService
      .query()
      .pipe(map((res: HttpResponse<ICategory[]>) => res.body ?? []))
      .pipe(
        map((categories: ICategory[]) =>
          this.categoryService.addCategoryToCollectionIfMissing(categories, this.editForm.get('category')!.value)
        )
      )
      .subscribe((categories: ICategory[]) => (this.categoriesSharedCollection = categories));
  
    }

  protected createFromForm(): IJoke {
    return {
      ...new Joke(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      content: this.editForm.get(['content'])!.value,
      rating: this.editForm.get(['rating'])!.value,
      category: this.editForm.get(['category'])!.value,
    };
  }
}
