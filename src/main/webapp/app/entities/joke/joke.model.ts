import { ICategory } from 'app/entities/category/category.model';

export interface IJoke {
  id?: number;
  title?: string;
  content?: string | null;
  rating?: number | null;
  category?: ICategory | null;
}

export class Joke implements IJoke {
  constructor(
    public id?: number,
    public title?: string,
    public content?: string | null,
    public rating?: number | null,
    public category?: ICategory | null
  ) {}
}

export function getJokeIdentifier(joke: IJoke): number | undefined {
  return joke.id;
}
