import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IJoke } from '../joke.model';
import { JokeService } from '../service/joke.service';

@Component({
  templateUrl: './joke-delete-dialog.component.html',
})
export class JokeDeleteDialogComponent {
  joke?: IJoke;

  constructor(protected jokeService: JokeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.jokeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
