import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IJoke, Joke } from '../joke.model';
import { JokeService } from '../service/joke.service';

@Injectable({ providedIn: 'root' })
export class JokeRoutingResolveService implements Resolve<IJoke> {
  constructor(protected service: JokeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IJoke> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((joke: HttpResponse<Joke>) => {
          if (joke.body) {
            return of(joke.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Joke());
  }
}
