import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { JokeComponent } from '../list/joke.component';
import { JokeDetailComponent } from '../detail/joke-detail.component';
import { JokeUpdateComponent } from '../update/joke-update.component';
import { JokeRoutingResolveService } from './joke-routing-resolve.service';

const jokeRoute: Routes = [
  {
    path: '',
    component: JokeComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: JokeDetailComponent,
    resolve: {
      joke: JokeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: JokeUpdateComponent,
    resolve: {
      joke: JokeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: JokeUpdateComponent,
    resolve: {
      joke: JokeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(jokeRoute)],
  exports: [RouterModule],
})
export class JokeRoutingModule {}
