import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IJoke, getJokeIdentifier } from '../joke.model';

export type EntityResponseType = HttpResponse<IJoke>;
export type EntityArrayResponseType = HttpResponse<IJoke[]>;

@Injectable({ providedIn: 'root' })
export class JokeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/jokes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(joke: IJoke): Observable<EntityResponseType> {
    return this.http.post<IJoke>(this.resourceUrl, joke, { observe: 'response' });
  }

  update(joke: IJoke): Observable<EntityResponseType> {
    return this.http.put<IJoke>(`${this.resourceUrl}/${getJokeIdentifier(joke) as number}`, joke, { observe: 'response' });
  }

  partialUpdate(joke: IJoke): Observable<EntityResponseType> {
    return this.http.patch<IJoke>(`${this.resourceUrl}/${getJokeIdentifier(joke) as number}`, joke, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IJoke>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IJoke[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addJokeToCollectionIfMissing(jokeCollection: IJoke[], ...jokesToCheck: (IJoke | null | undefined)[]): IJoke[] {
    const jokes: IJoke[] = jokesToCheck.filter(isPresent);
    if (jokes.length > 0) {
      const jokeCollectionIdentifiers = jokeCollection.map(jokeItem => getJokeIdentifier(jokeItem)!);
      const jokesToAdd = jokes.filter(jokeItem => {
        const jokeIdentifier = getJokeIdentifier(jokeItem);
        if (jokeIdentifier == null || jokeCollectionIdentifiers.includes(jokeIdentifier)) {
          return false;
        }
        jokeCollectionIdentifiers.push(jokeIdentifier);
        return true;
      });
      return [...jokesToAdd, ...jokeCollection];
    }
    return jokeCollection;
  }
}
