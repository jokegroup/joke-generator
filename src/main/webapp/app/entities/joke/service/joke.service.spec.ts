import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IJoke, Joke } from '../joke.model';

import { JokeService } from './joke.service';

describe('Joke Service', () => {
  let service: JokeService;
  let httpMock: HttpTestingController;
  let elemDefault: IJoke;
  let expectedResult: IJoke | IJoke[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(JokeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      title: 'AAAAAAA',
      content: 'AAAAAAA',
      rating: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Joke', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Joke()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Joke', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          content: 'BBBBBB',
          rating: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Joke', () => {
      const patchObject = Object.assign(
        {
          title: 'BBBBBB',
          rating: 1,
        },
        new Joke()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Joke', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          content: 'BBBBBB',
          rating: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Joke', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addJokeToCollectionIfMissing', () => {
      it('should add a Joke to an empty array', () => {
        const joke: IJoke = { id: 123 };
        expectedResult = service.addJokeToCollectionIfMissing([], joke);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(joke);
      });

      it('should not add a Joke to an array that contains it', () => {
        const joke: IJoke = { id: 123 };
        const jokeCollection: IJoke[] = [
          {
            ...joke,
          },
          { id: 456 },
        ];
        expectedResult = service.addJokeToCollectionIfMissing(jokeCollection, joke);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Joke to an array that doesn't contain it", () => {
        const joke: IJoke = { id: 123 };
        const jokeCollection: IJoke[] = [{ id: 456 }];
        expectedResult = service.addJokeToCollectionIfMissing(jokeCollection, joke);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(joke);
      });

      it('should add only unique Joke to an array', () => {
        const jokeArray: IJoke[] = [{ id: 123 }, { id: 456 }, { id: 78332 }];
        const jokeCollection: IJoke[] = [{ id: 123 }];
        expectedResult = service.addJokeToCollectionIfMissing(jokeCollection, ...jokeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const joke: IJoke = { id: 123 };
        const joke2: IJoke = { id: 456 };
        expectedResult = service.addJokeToCollectionIfMissing([], joke, joke2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(joke);
        expect(expectedResult).toContain(joke2);
      });

      it('should accept null and undefined values', () => {
        const joke: IJoke = { id: 123 };
        expectedResult = service.addJokeToCollectionIfMissing([], null, joke, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(joke);
      });

      it('should return initial array if no Joke is added', () => {
        const jokeCollection: IJoke[] = [{ id: 123 }];
        expectedResult = service.addJokeToCollectionIfMissing(jokeCollection, undefined, null);
        expect(expectedResult).toEqual(jokeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
