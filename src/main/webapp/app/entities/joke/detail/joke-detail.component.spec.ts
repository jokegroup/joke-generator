import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { JokeDetailComponent } from './joke-detail.component';

describe('Joke Management Detail Component', () => {
  let comp: JokeDetailComponent;
  let fixture: ComponentFixture<JokeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [JokeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ joke: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(JokeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(JokeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load joke on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.joke).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
