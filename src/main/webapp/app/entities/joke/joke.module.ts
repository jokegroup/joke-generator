import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { JokeComponent } from './list/joke.component';
import { JokeDetailComponent } from './detail/joke-detail.component';
import { JokeUpdateComponent } from './update/joke-update.component';
import { JokeDeleteDialogComponent } from './delete/joke-delete-dialog.component';
import { JokeModalComponent } from './modal/joke-modal.component';
import { JokeRoutingModule } from './route/joke-routing.module';

@NgModule({
  imports: [SharedModule, JokeRoutingModule],
  declarations: [JokeComponent,JokeModalComponent,JokeDetailComponent, JokeUpdateComponent, JokeDeleteDialogComponent],
  entryComponents: [JokeDeleteDialogComponent],
})
export class JokeModule {}
