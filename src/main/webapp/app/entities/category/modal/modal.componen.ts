import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from '../service/category.service';
import { ActivatedRoute,Router} from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import {  AlertService } from 'app/core/util/alert.service';
import { ICategory, Category } from '../category.model';

@Component({
  selector: 'jhi-modal',
  templateUrl: './modal.component.html',
})
export class ModalComponent {
  x:any
    isSaving = false;
    categories?: ICategory[];
    category: ICategory | null = null;
    cat:any;
    editForm = this.fb.group({
      id: [],
      name: [null, [Validators.required, Validators.maxLength(255)]],
    });
  constructor(private alertService: AlertService, protected router: Router,private activeModal: NgbActiveModal,protected categoryService: CategoryService,protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  

  dismiss(): void {
    this.activeModal.dismiss();
  }
  // mod():void{

  // }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    
    this.isSaving = true;
    this.categoryService.getAll().subscribe( category => {
      this.cat=category;
 
      const categoryu = this.createFromForm();
      const idx: number = category.findIndex(obj => obj.name === categoryu.name);
        if(idx > -1 ){   
          this.router.navigate(['/joke/new'])     
          this.dismiss();
          
        }
    
else{
  this.subscribeToSaveResponse(this.categoryService.create(categoryu));  
     this.randomMethod();
    
  }
    });
  }
  

  randomMethod():void {
    this.alertService.get().push(
     this.alertService.addAlert(
         {
             type: 'warning',
             message: 'This Category doesn t exist',
             timeout: 0.001,
             toast: false,
             
         },
        
        
     )
     
 );
        }

       
  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICategory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.dismiss(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(category: ICategory): void {
    this.editForm.patchValue({
      id: category.id,
      name: category.name,
    });
  }

  protected createFromForm(): ICategory {
    return {
      ...new Category(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
    };
  }
}
