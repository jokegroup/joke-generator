import { IJoke } from 'app/entities/joke/joke.model';

export interface ICategory {
  id?: number;
  name?: string;
  jokes?: IJoke[] | null;
}

export class Category implements ICategory {
  constructor(public id?: number, public name?: string, public jokes?: IJoke[] | null) {}
}

export function getCategoryIdentifier(category: ICategory): number | undefined {
  return category.id;
}
