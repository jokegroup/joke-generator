import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'joke',
        data: { pageTitle: 'jokegeneratorApp.joke.home.title' },
        loadChildren: () => import('./joke/joke.module').then(m => m.JokeModule),
      },
      {
        path: 'category',
        data: { pageTitle: 'jokegeneratorApp.category.home.title' },
        loadChildren: () => import('./category/category.module').then(m => m.CategoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
