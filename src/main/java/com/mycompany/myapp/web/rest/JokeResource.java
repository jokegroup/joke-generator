package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Joke;
import com.mycompany.myapp.repository.JokeRepository;
import com.mycompany.myapp.service.JokeService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Joke}.
 */
@RestController
@RequestMapping("/api")
public class JokeResource {

    private final Logger log = LoggerFactory.getLogger(JokeResource.class);

    private static final String ENTITY_NAME = "joke";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JokeService jokeService;

    private final JokeRepository jokeRepository;

    public JokeResource(JokeService jokeService, JokeRepository jokeRepository) {
        this.jokeService = jokeService;
        this.jokeRepository = jokeRepository;
    }

    /**
     * {@code POST  /jokes} : Create a new joke.
     *
     * @param joke the joke to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new joke, or with status {@code 400 (Bad Request)} if the joke has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/jokes")
    public ResponseEntity<Joke> createJoke(@Valid @RequestBody Joke joke) throws URISyntaxException {
        log.debug("REST request to save Joke : {}", joke);
        if (joke.getId() != null) {
            throw new BadRequestAlertException("A new joke cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Joke result = jokeService.save(joke);
        return ResponseEntity
            .created(new URI("/api/jokes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /jokes/:id} : Updates an existing joke.
     *
     * @param id the id of the joke to save.
     * @param joke the joke to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated joke,
     * or with status {@code 400 (Bad Request)} if the joke is not valid,
     * or with status {@code 500 (Internal Server Error)} if the joke couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/jokes/{id}")
    public ResponseEntity<Joke> updateJoke(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Joke joke)
        throws URISyntaxException {
        log.debug("REST request to update Joke : {}, {}", id, joke);
        if (joke.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, joke.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!jokeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Joke result = jokeService.update(joke);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, joke.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /jokes/:id} : Partial updates given fields of an existing joke, field will ignore if it is null
     *
     * @param id the id of the joke to save.
     * @param joke the joke to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated joke,
     * or with status {@code 400 (Bad Request)} if the joke is not valid,
     * or with status {@code 404 (Not Found)} if the joke is not found,
     * or with status {@code 500 (Internal Server Error)} if the joke couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/jokes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Joke> partialUpdateJoke(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Joke joke
    ) throws URISyntaxException {
        log.debug("REST request to partial update Joke partially : {}, {}", id, joke);
        if (joke.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, joke.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!jokeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Joke> result = jokeService.partialUpdate(joke);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, joke.getId().toString())
        );
    }

    /**
     * {@code GET  /jokes} : get all the jokes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of jokes in body.
     */
    @GetMapping("/jokes")
    public ResponseEntity<List<Joke>> getAllJokes(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Jokes");
        Page<Joke> page = jokeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /jokes/:id} : get the "id" joke.
     *
     * @param id the id of the joke to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the joke, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/jokes/{id}")
    public ResponseEntity<Joke> getJoke(@PathVariable Long id) {
        log.debug("REST request to get Joke : {}", id);
        Optional<Joke> joke = jokeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(joke);
    }

    /**
     * {@code DELETE  /jokes/:id} : delete the "id" joke.
     *
     * @param id the id of the joke to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/jokes/{id}")
    public ResponseEntity<Void> deleteJoke(@PathVariable Long id) {
        log.debug("REST request to delete Joke : {}", id);
        jokeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
