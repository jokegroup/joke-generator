package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Joke;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Joke entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JokeRepository extends JpaRepository<Joke, Long> {}
