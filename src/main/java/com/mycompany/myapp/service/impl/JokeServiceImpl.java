package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.domain.Joke;
import com.mycompany.myapp.repository.JokeRepository;
import com.mycompany.myapp.service.JokeService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Joke}.
 */
@Service
@Transactional
public class JokeServiceImpl implements JokeService {

    private final Logger log = LoggerFactory.getLogger(JokeServiceImpl.class);

    private final JokeRepository jokeRepository;

    public JokeServiceImpl(JokeRepository jokeRepository) {
        this.jokeRepository = jokeRepository;
    }

    @Override
    public Joke save(Joke joke) {
        log.debug("Request to save Joke : {}", joke);
        return jokeRepository.save(joke);
    }

    @Override
    public Joke update(Joke joke) {
        log.debug("Request to save Joke : {}", joke);
        return jokeRepository.save(joke);
    }

    @Override
    public Optional<Joke> partialUpdate(Joke joke) {
        log.debug("Request to partially update Joke : {}", joke);

        return jokeRepository
            .findById(joke.getId())
            .map(existingJoke -> {
                if (joke.getTitle() != null) {
                    existingJoke.setTitle(joke.getTitle());
                }
                if (joke.getContent() != null) {
                    existingJoke.setContent(joke.getContent());
                }
                if (joke.getRating() != null) {
                    existingJoke.setRating(joke.getRating());
                }

                return existingJoke;
            })
            .map(jokeRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Joke> findAll(Pageable pageable) {
        log.debug("Request to get all Jokes");
        return jokeRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Joke> findOne(Long id) {
        log.debug("Request to get Joke : {}", id);
        return jokeRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Joke : {}", id);
        jokeRepository.deleteById(id);
    }
}
