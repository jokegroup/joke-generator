package com.mycompany.myapp.service;

import com.mycompany.myapp.domain.Joke;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link Joke}.
 */
public interface JokeService {
    /**
     * Save a joke.
     *
     * @param joke the entity to save.
     * @return the persisted entity.
     */
    Joke save(Joke joke);

    /**
     * Updates a joke.
     *
     * @param joke the entity to update.
     * @return the persisted entity.
     */
    Joke update(Joke joke);

    /**
     * Partially updates a joke.
     *
     * @param joke the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Joke> partialUpdate(Joke joke);

    /**
     * Get all the jokes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Joke> findAll(Pageable pageable);

    /**
     * Get the "id" joke.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Joke> findOne(Long id);

    /**
     * Delete the "id" joke.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
